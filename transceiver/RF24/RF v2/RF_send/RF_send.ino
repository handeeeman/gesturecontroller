#include <SPI.h>  
#include "RF24.h"
#include "printf.h"
#include "string.h"

RF24 myRadio (7, 8);
byte addresses[][6] = {"0"};

struct package
{
  int id=1;
  char  text[100] = "Hello CoE 197W - THX!";
};


typedef struct package Package;
Package data;


void setup()
{
  Serial.begin(115200);
  delay(1000);
  myRadio.begin();  
  myRadio.setChannel(115); 
  myRadio.setPALevel(RF24_PA_MAX);
  myRadio.setDataRate( RF24_250KBPS ) ; 
  myRadio.openWritingPipe( addresses[0]);
  delay(1000);
  printf_begin();
  Serial.println("Ready to Transmit. My details are: ");
  myRadio.printDetails();
  Serial.print("Am i an NRF24L01?: ");
  Serial.println(myRadio.isPVariant()); 

  
}

void loop()
{
  if(Serial.available())
  {
  //myRadio.write(&data, sizeof(data));
 // data.text = (char)Serial.read(); 
  data.id = Serial.parseInt();
  //  Serial.print("\nPackage:");
    Serial.println(data.id);
  //  Serial.print("\n");
  //  Serial.println(data.temperature);
    //Serial.println(data.text);
   // Serial.print("\n");
   // data.id = data.id + 1;
  //  data.temperature = data.temperature+0.1;
  myRadio.write(&data, sizeof(data)); 
    delay(1000);
    
  }

}
