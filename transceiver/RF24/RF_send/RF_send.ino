#include <SPI.h>  
#include "RF24.h"

RF24 myRadio (7, 8);
byte addresses[][6] = {"0"};

struct package
{
  int id=1;
  char  text[100] = "Hello CoE 197W - THX!";
};


typedef struct package Package;
Package data;


void setup()
{
  Serial.begin(115200);
  delay(1000);
  myRadio.begin();  
  myRadio.setChannel(115); 
  myRadio.setPALevel(RF24_PA_MAX);
  myRadio.setDataRate( RF24_250KBPS ) ; 
  myRadio.openWritingPipe( addresses[0]);
  delay(1000);
}

void loop()
{
  myRadio.write(&data, sizeof(data)); 

//  Serial.print("\nPackage:");
  Serial.println(data.id);
//  Serial.print("\n");
//  Serial.println(data.temperature);
  Serial.println(data.text);
  Serial.print("\n");
  data.id = data.id + 1;
//  data.temperature = data.temperature+0.1;
  delay(1000);

}
